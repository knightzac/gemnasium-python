package pkgmngr

import (
	"fmt"
	"os"
	"os/exec"

	log "github.com/sirupsen/logrus"
)

type pip struct {
	projectPath string
	depFile     string
}

func (pm pip) DepFile() string {
	return pm.depFile
}

func (pm pip) InstallDependencies(opts InstallOptions) error {
	if opts.PipCertPath != "" {
		// Force value of PIP_CERT based on options.
		// Note that PIP_CERT might be defined in the CLI environment.
		if err := os.Setenv("PIP_CERT", opts.PipCertPath); err != nil {
			return err
		}
	}

	if opts.PipVersion != "" {
		if err := pm.installPip(opts.PipVersion); err != nil {
			return err
		}
	}

	// fetch dependencies unless dependencyPath is already set
	if !opts.SkipFetch {
		cmd := exec.Command("pip", "download", "--disable-pip-version-check", "--dest", opts.CacheDir, "-r", pm.DepFile())
		output, err := setupCmd(pm.projectPath, cmd).CombinedOutput()
		log.Debugf("%s\n%s", cmd.String(), output)

		if err != nil {
			return err
		}
	}

	args := []string{
		"install",
		"--disable-pip-version-check",
		"--find-links", opts.CacheDir,
		"--requirement", pm.DepFile(),
	}

	// When dependencies are passed explicitly, rely entirely off `find-links` path
	if opts.SkipFetch {
		args = append(args, "--no-index")
	}

	cmd := exec.Command("pip", args...)
	output, err := setupCmd(pm.projectPath, cmd).CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)

	return err
}

func (pm pip) DependencyGraph() ([]byte, error) {
	cmd := exec.Command("pipdeptree", "--json")
	cmd.Stderr = os.Stderr
	return setupCmd(pm.projectPath, cmd).Output()
}

func (pm pip) installPip(version string) error {
	versionString := fmt.Sprintf(`pip==%s`, version)
	cmd := exec.Command("python", "/get-pip.py", "--disable-pip-version-check", "--no-cache-dir", versionString)
	cmd.Env = os.Environ()
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)
	return err
}
