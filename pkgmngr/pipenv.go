package pkgmngr

import (
	"os"
	"os/exec"

	log "github.com/sirupsen/logrus"
)

type pipenv struct {
	projectPath string
}

func (pm pipenv) DepFile() string {
	return "Pipfile"
}

func (pm pipenv) InstallDependencies(opts InstallOptions) error {
	if opts.PipCertPath != "" {
		// Force value of PIP_CERT based on options.
		// Note that PIP_CERT might be defined in the CLI environment.
		if err := os.Setenv("PIP_CERT", opts.PipCertPath); err != nil {
			return err
		}
	}

	// TODO: We'd better install the required python version instead of forcing
	// to use the one available in this container.
	args := []string{
		"install",
		"--python", "/usr/local/bin/python",
	}

	cmd := exec.Command("pipenv", args...)
	output, err := setupCmd(pm.projectPath, cmd).CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)

	return err
}

func (pm pipenv) DependencyGraph() ([]byte, error) {
	cmd := exec.Command("pipenv", "graph", "--json")
	cmd.Stderr = os.Stderr
	return setupCmd(pm.projectPath, cmd).Output()
}
