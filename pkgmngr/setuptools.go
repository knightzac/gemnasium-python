package pkgmngr

import (
	"os"
	"os/exec"

	log "github.com/sirupsen/logrus"
)

type setuptools struct {
	projectPath string
}

func (pm setuptools) DepFile() string {
	return "setup.py"
}

func (pm setuptools) InstallDependencies(opts InstallOptions) error {
	cmd := setupCmd(pm.projectPath, exec.Command("/usr/local/bin/python", "setup.py", "install"))
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)

	return err
}

func (pm setuptools) DependencyGraph() ([]byte, error) {
	cmd := exec.Command("pipdeptree", "--json")
	cmd.Stderr = os.Stderr
	return setupCmd(pm.projectPath, cmd).Output()
}
