package metadata

import (
	"fmt"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

const (
	// AnalyzerVendor is the vendor/maintainer of the analyzer
	AnalyzerVendor = "GitLab"

	analyzerID = "gemnasium-python"

	// AnalyzerName is the name of the analyzer
	AnalyzerName = analyzerID

	scannerVendor = AnalyzerVendor
	scannerURL    = "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python"

	// Type returns the type of the scan
	Type issue.Category = issue.CategoryDependencyScanning
)

var (
	// AnalyzerVersion is the semantic version of the analyzer and must match the most recent version in CHANGELOG.md
	AnalyzerVersion = "2.15.0"

	// ScannerVersion is the semantic version of the scanner
	ScannerVersion = AnalyzerVersion

	// ReportScanner returns identifying information about a security scanner
	ReportScanner = issue.ScannerDetails{
		ID:      analyzerID,
		Name:    AnalyzerName,
		Version: ScannerVersion,
		Vendor: issue.Vendor{
			Name: scannerVendor,
		},
		URL: scannerURL,
	}

	// AnalyzerUsage provides a one line usage string for the analyzer
	AnalyzerUsage = fmt.Sprintf("%s %s analyzer v%s", AnalyzerVendor, AnalyzerName, AnalyzerVersion)
)
