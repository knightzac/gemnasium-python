FROM python:3.6-slim

ENV VRANGE_DIR="/vrange"
ARG GEMNASIUM_VRANGE_BRANCH="v2.3.0"
ARG GEMNASIUM_VRANGE_REPO="https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium"
ARG GEMNASIUM_VRANGE_URL="$GEMNASIUM_VRANGE_REPO/raw/$GEMNASIUM_VRANGE_BRANCH/vrange"

ARG GEMNASIUM_DB_LOCAL_PATH="/gemnasium-db"
ARG GEMNASIUM_DB_REMOTE_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db.git"
ARG GEMNASIUM_DB_WEB_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db"
ARG GEMNASIUM_DB_REF_NAME="master"

ENV GEMNASIUM_DB_LOCAL_PATH $GEMNASIUM_DB_LOCAL_PATH
ENV GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_REMOTE_URL
ENV GEMNASIUM_DB_WEB_URL $GEMNASIUM_DB_WEB_URL
ENV GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REF_NAME

ADD get-pip.py requirements.txt /

RUN \
	# include gcc build env
	apt-get update && \
	apt-get install -y wget git build-essential libssl-dev libffi-dev musl-dev python3-dev && \
	apt-get clean && \
	\
	# install pipenv and its requirements
	pip install -r /requirements.txt && \
	\
	# install vrange/python
	mkdir -p $VRANGE_DIR/python && \
	wget -O $VRANGE_DIR/python/rangecheck.py $GEMNASIUM_VRANGE_URL/python/rangecheck.py && \
	wget -O $VRANGE_DIR/python/requirements.txt $GEMNASIUM_VRANGE_URL/python/requirements.txt && \
	chmod +x $VRANGE_DIR/python/rangecheck.py && \
	pip install -r $VRANGE_DIR/python/requirements.txt && \
	\
	# clone gemnasium-db
	git clone --branch $GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_LOCAL_PATH

COPY analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
